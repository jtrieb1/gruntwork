# handler.py
# This defines the various file handlers used by gruntwork.

import pandas as pd
import os

class Handler(object):
    def __init__(self, workingfile, *args):
        self.wfile = workingfile
        self.log = []
        self.filetype = None
        try:
            self.df = args[0]
        except:
            self.df = None
        self.hist = [self.df]

    def getcols(self):
        if self.df is not None:
            return self.df.columns.values.tolist()

    def __str__(self):
        return str(self.wfile) + " Handler"

    def __repr__(self):
        return self.__str__()

class CSVHandler(Handler):
    def __init__(self, workingfile, *args):
        super(CSVHandler, self).__init__(workingfile, *args)
        try:
            self.df = pd.read_csv(os.path.join(os.getcwd(),workingfile))
        except:
            try:
                self.df = args[0]
            except:
                self.df = None
        self.hist = [self.df]


class XLSXHandler(Handler):
    def __init__(self, workingfile, *args):
        super(XLSXHandler, self).__init__(workingfile, *args)
        try:
            self.dfs = []
            blanks = int(raw_input("How many empty rows does " + workingfile + " begin with? "))
            sheets = pd.read_excel(os.path.join(os.getcwd(),workingfile),sheet_name=None, skiprows=blanks)
            for index, sheet in sheets.items():
                self.dfs.append((workingfile[:-5] + '-' + str(index) + '.xlsx',sheet))            
        except:
            self.dfs = None
