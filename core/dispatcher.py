# dispatcher.py
# Contains the dispatcher class for management of handlers.

from handler import Handler, CSVHandler, XLSXHandler
from configurer import Config
from utils.modifier import Modifier
from utils.analyzer import Analyzer
import os

class Dispatcher(object):
    def __init__(self):
        self.handlers = []
        self.modifiers = []
        self.analyzers = []
        self.configs = []
        self.working = None
        self.toolbox = None
        self.config = None
        self.analyzer = None
        self.session = 0

    def addHandler(self, handler):
        self.handlers.append(handler)
        self.configs.append(Config(handler))
        self.modifiers.append(Modifier(handler))
        self.analyzers.append(Analyzer(handler))
        handler.df.to_csv(os.path.join(os.getcwd(), 'backup', str(self.session), os.path.splitext(handler.wfile)[0] + '.csv'),index=False)

    def list_active(self):
        return [x.wfile for x in self.handlers]

    def load(self, name):
        if name:
            try:
                if name not in self.list_active():
                    if name[-4:] == '.csv':
                        self.addHandler(CSVHandler(name))
                    elif name[-5:] == '.xlsx':
                        xlsxHandler = XLSXHandler(name)
                        for sheet in xlsxHandler.dfs:
                            self.addHandler(Handler(sheet[0],sheet[1]))
                    self.working = self.handlers[-1]
                    self.toolbox = self.modifiers[-1]
                    self.config = self.configs[-1]
                    self.analyzer = self.analyzers[-1]
                    return
                else:
                    return
            except:
                print "File does not exist."
                return
        raise Exception

    def switch(self, name):
        chosen = [x for x in self.handlers if x.wfile == name]
        if chosen:
            self.working = chosen[0]
            self.toolbox = [x for x in self.modifiers if x.handler == self.working][0]
            self.config = [x for x in self.configs if x.handler == self.working][0]
            self.analyzer = [x for x in self.analyzers if x.handler == self.working][0]
        else:
            print "File not in memory."

    def __str__(self):
        return "Dispatcher"

    def __repr__(self):
        return self.__str__()
