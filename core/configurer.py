# Here's how we handle configuration.

import os, platform
from handler import Handler

clearcom = "clear"
if platform.system() == 'Windows':
    clearcom = 'cls'

class Config(object):
    def __init__(self, handler):
        self.dist = {"Unique": [],
                     "Date": [],
                     "Phone": [],
                     "Populate": [],
                     "String": []}
        self.corr = {}
        self.handler = handler
        if self.handler.wfile[:-4]+"-dist.conf" in os.listdir(os.getcwd()):
            self.load_dist(handler.wfile[:-4]+'-dist.conf')
        if self.handler.wfile[:-4]+"-corr.conf" in os.listdir(os.getcwd()):
            self.load_corr(handler.wfile[:-4]+'-corr.conf')

    def print_dist(self):
        for key, val in self.dist.items():
            print key + ': ' + ', '.join(val)

    def print_corr(self):
        for key, val in self.corr.items():
            print '\n' + key + ':'
            for key2, val2 in val.items():
                print '\t' + key2 + ': ' + val2
            

    def load_dist(self, name):
        try:
            with open(name, 'r') as d:
                for line in d:
                    llist = line.strip().split(';')
                    self.dist[llist[0].strip()] = [x.strip() for x in llist[1].split(',')]
            self.handler.log.append('config')
            print "Successfully loaded distinguishing file: " + name
            return
        except:
            print "Dist file not formatted correctly!"
            return

    def load_corr(self, name):
        label = ""
        try:
            with open(name, 'r') as c:
                for line in c:
                    llist = line.strip().split(';')
                    if len(llist) == 1:
                        label = llist[0]
                        self.corr[label] = {}
                    else:
                        self.corr[label][llist[0]] = llist[1].strip()
            print "Successfully loaded corrections file: " + name
            return
        except:
            self.corr = {}
            print "Corrections file not found."
            return

    def input_dist(self):
        columns = self.handler.getcols()
        for key, val in self.dist.items():
            os.system(clearcom)
            print 'Columns:'
            for column in columns:
                print '\t' + str(columns.index(column) + 1) + ': ' + column
            print
            print "At each prompt, select the column headers corresponding to the properties."
            print "Unique fields are fields that can be used to cluster records by identity."
            print "Fields are populated based on shared unique fields."
            print
            try:
                self.dist[key] = [columns[int(x)-1] for x in raw_input(str(key) + " fields? ").split(',')]
            except:
                continue
        # Save it for later
        with open(os.path.join(os.getcwd(),os.path.splitext(self.handler.wfile)[0]+'-dist.conf'), 'w') as o:
            d_str = "Unique; " + ', '.join(self.dist["Unique"]) + "\nDate; " + ', '.join(self.dist["Date"]) + "\nPhone; " + ', '.join(self.dist["Phone"]) + "\nPopulate; " + ', '.join(self.dist["Populate"]) + "\nString; " + ', '.join(self.dist["String"]) + "\n"
            o.write(d_str)
        
    def input_corr(self):
        os.system(clearcom)
        print "Input which columns need corrections, then input the necessary corrections for each column.\n"
        print 'Columns:'
        for name in self.handler.getcols():
            print '\t' + str(self.handler.getcols().index(name) + 1) + ': ' + name
        print
        columns = [self.handler.getcols()[int(x)-1] for x in raw_input("Columns? ").split(',')]
        corr_str = []
        for column in columns:
            if column:
                corr_str.append(column + "\n")
                while True:
                    os.system(clearcom)
                    print "Input which columns need corrections, then input the necessary corrections for each column.\n"
                    i = raw_input(column + ": What needs to change? (Type exit to move to next column)\n")
                    if i.lower() != "exit":
                        o = raw_input("To what?\n")
                        corr_str.append(i + "; " + o + "\n")
                    else:
                        break
                with open(os.path.join(os.getcwd(),os.path.splitext(self.handler.wfile)[0]+'-corr.conf'), 'w') as o:
                    o.write("".join(corr_str))
            self.load_corr(os.path.splitext(self.handler.wfile)[0]+'-corr.conf')
