# modifier.py
# This class contains the transformative methods used by gruntwork.

import numpy as np
import pandas as pd
from halo import Halo

pd.options.mode.chained_assignment = None

class Modifier(object):
    
    def __init__(self, handler):
        self.handler = handler
        
    def correct(self, corrections):
        for column, swaps in corrections.items():
            for initial, final in swaps.items():
                self.handler.df[column].replace(initial, final, inplace=True)
        self.handler.hist.append(self.handler.df.copy())
        self.handler.log.append('correct')

    def dateformat(self, col):
        if col != 0:
            if len(col.split('/')[2]) != 4:
                f_date = col[:-2]+'20'+col[-2:]
            else:
                f_date = col
            return pd.to_datetime(f_date, format='%m/%d/%Y', errors='coerce')
        return 0
        
    def phoneformat(self, x):
        f_str = ''.join(c for c in x if c not in [' ','(',')','-'])
        if len(f_str) <= 10:
            return f_str[:3]+'-'+f_str[3:6]+'-'+f_str[6:]
        elif "Int: +" not in x:
            return "Int: +"+f_str[:-10]+'-'+f_str[-10:-7]+'-'+f_str[-7:-4]+'-'+f_str[-4:]
        else:
            return x

    def dropempty(self):
        self.handler.df = self.handler.df.loc[:, ~self.handler.df.columns.str.contains('^Unnamed')]
    
    @Halo(text='Cleaning file', spinner='dots')
    def clean(self, config):

        df = self.handler.df
        
        # Remove unnecessary columns
        df = df.loc[:, ~df.columns.str.contains('^Unnamed')]
        # Remove empty rows
        df.dropna(axis=0, how='all', inplace=True)

        # Put those config files to good use
        for col in config.dist["String"]:
            df[col].fillna('',inplace=True)
            df[col] = df[col].str.decode('iso-8859-1').str.encode('utf-8')
        
        # Initial populate pass
        if config.dist["Unique"]:
            for identity, data in df.groupby(config.dist["Unique"][0]):
                if identity:
                    for item in config.dist["Populate"]:
                        if data[data[item].notnull()][item].any():
                            df.loc[df[config.dist["Unique"][0]] == identity,item] = data[data[item].notnull()][item].sort_values(ascending=False).iloc[0]
        # Fix dates
        for col in config.dist["Date"]:
            try:
                df[col] = pd.to_datetime(df[col])
            except:
                df[col].fillna(0,inplace=True)
                df[col] = df[col].apply(self.dateformat)
                df[col].replace(0,pd.NaT,inplace=True)
        if config.dist["Date"]:
            df.sort_values(by=config.dist["Date"][0],ascending=False, inplace=True)

        # Fix phone numbers
        for col in config.dist["Phone"]:
            df[col].fillna(' ',inplace=True)
            df[col] = df[col].apply(self.phoneformat)
            df[col].replace('--',np.nan,inplace=True)

        #Final populate pass
        try:
            for element in config.dist["Unique"][1:]:
                for identity, data in df.groupby(element):
                    if identity:
                        if data[data[config.dist["Unique"][0]].notnull()][config.dist["Unique"][0]].any():
                            df.loc[df[element] == identity,config.dist["Unique"][0]] = data[data[config.dist["Unique"][0]].notnull()][config.dist["Unique"][0]].sort_values(ascending=False).iloc[0]
        except:
            pass

        # Log cleaning
        self.handler.hist.append(df.copy())
        self.handler.log.append('clean')
        return df

    def singleton(self, column):
        singleton = self.handler.df[(self.handler.df.groupby(column).cumcount() == 0)]
        duplicate = self.handler.df[(self.handler.df.groupby(column).cumcount() != 0)]
        self.handler.log.append('singleton '+str(column))
        return (singleton, duplicate)
        

    def split(self, column):
        newframes = []
        df = self.handler.df
        for val in df[column].apply(str).unique():
            if val:
                newframes.append((df[df[column].apply(str) == val], str(val)))
            else:
                newframes.append((df[df[column].apply(str) == val], "Null"))
        # newframes :: list of tuples (dataframe, name)
        self.handler.log.append('split '+str(column))
        for frame in newframes:
            frame[0].drop(columns=[column], axis=1, inplace=True)
        return newframes

    def __str__(self):
        return str(self.handler.wfile) + " Modifier"

    def __repr__(self):
        return self.__str__()
