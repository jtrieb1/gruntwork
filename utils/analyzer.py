# analyzer.py
# This file contains methods for statistical analysis of the
# spreadsheets created by other modules.

import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

class Analyzer(object):

    def __init__(self, handler):
        self.handler = handler


    def simplescatter(self,x,y):
        plot = sns.lmplot(x,y,data=self.handler.df,fit_reg=False)
        plot.set_xticklabels(rotation=90)
        plt.show()

    # Old implementation, need something better.   
    '''
    def stats(self, fields, times, ops, flags, config, save=False):
        # Needs flexibility of units.
        # fields : column names of interest :: list[str]
        # times : years of interest :: list[int]
        # ops: names of operations and implementations :: (str, np.__)
        # flags: list of column headers of interest in describe() :: list[str]
        # config: just pass config object from dispatcher
        # save: whether or not to save to file :: Bool
        dist = config.dist
        if not dist["Date"]:
            print "Data without a datetime field is currently unsupported."
            return
        df = self.handler.df
        if not save:
            os.system('cls')
        for item in fields:
            desc = df[item].groupby([df[dist["Date"][0]].dt.year, df[dist["Date"][0]].dt.month]).describe()
            for flag in flags:
                years = list(set([int(y) for y in times]))
                years.sort()
                print flag[0].upper()+flag[1:]+':\n'
                for year in years:
                    print '\t' + str(year) + ':'
                    for op in ops:
                        print '\t\t' + op[0] + ':'
                        print '\t\t\t' + str(desc[flag][year].agg(op[1]))
                    print
                    months = list(set([int(y) for y,v in desc[flag][year].items()]))
                    months.sort
                    stdv = desc[flag][year].agg(np.std)
                    mean = desc[flag][year].agg(np.mean)
                    threshold = (mean-stdv,mean+stdv)
                    print "\tStatistically significant months:"
                    for month in months:
                        if desc[flag][year][month]:
                            if desc[flag][year][month] < threshold[0]:
                                print '\t\t' + str(month) + ': Low'
                            elif desc[flag][year][month] > threshold[1]:
                                print '\t\t' + str(month) + ': High'
                    print
                    print "\tZ-Scores:\n"
                    for month in months:
                        print '\t\t' + str(month) + ': ' + str((desc[flag][year][month]-mean)/stdv)

    def freqgraph(self, fields, times, cols, config, save=False):
        # This is far from general. Needs flexibility of units, among other things.
        # fields :: list[str]
        # times :: list[int]
        # cols : columns of .describe() to plot :: list[str]
        # config :: Config()

        scale = (False, 'standard')
        if not config.dist["Date"]:
            print "The data must have a datetime field for frequency analysis."
            return
        flag = raw_input("Graph on logarithmic scale? ").lower()[0]
        if flag == 'y':
            scale = (True, 'log')
        for field in fields:
            for year in times:
                for col in cols:
                    try:
                        plt.figure()
                        df[field].groupby(df[config.dist["Date"][0]].dt.year, df[config.dist["Date"][0].dt.month]).describe()[col][year].plot(kind='bar',logy=scale[0])
                        if save:
                            plt.savefig(self.handler.wfile[:-4]+'-'+field+'-'+str(year)+scale[1]+'.png',bbox_inches='tight',pad_inches=0.5)
                        plt.show()
                    except:
                        print "An error has occurred."

    '''
    def __str__(self):
        return self.handler.wfile + ' Analyzer'

    def __repr__(self):
        return self.__str__()
