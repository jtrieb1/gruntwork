# gruntwork.py
#
# This is a shell interface for the various data tools I've developed, with
# an aim towards being client-friendly and intuitive.
import cmd, os, errno, platform
from core.dispatcher import Dispatcher
from core.handler import CSVHandler
import pandas as pd

clearcom = "clear"
if platform.system() == 'Windows':
    clearcom = 'cls'

class GruntShell(cmd.Cmd):
    prompt = "[+++]> "
    intro = "gruntwork: A simple interface for spreadsheet manipulation.\n\n"

    dispatcher = Dispatcher()
    sessions = []
    try:
        for f in os.listdir(os.path.join(os.getcwd(), 'backup')):
            try:
                sessions.append(int(f))
            except:
                pass
    except:
        pass
    if sessions:
        dispatcher.session = max(sessions) + 1
    else:
        dispatcher.session = 0

    def do_exit(self, args):
        '''\tExits from the console.'''
        return -1

    def do_loadconf(self, line):
        '''\tLoads a .conf file as a configuration for the current working file.\n\tWill find .conf files in any subdirectories of current directory.'''
        os.system(clearcom)
        print "Configuration files found:"
        cfiles = []
        for root, dirs, files in os.walk(os.getcwd()):
            for f in files:
                if os.path.splitext(f)[-1] == '.conf':
                    cfiles.append(os.path.relpath(os.path.join(root, f)))
        for f in cfiles:
            print str(cfiles.index(f) + 1) + ": " + f
        print
        choice = raw_input("Which file? ")
        kind = raw_input("Dist or Corr? (d/c) ")
        for e in [x.strip() for x in choice.split(',')]:
            try:
                n = int(e)
                name = cfiles[n-1]
            except:
                name = e
        if kind.lower()[0] == 'c':
            self.dispatcher.config.load_corr(name)
        elif kind.lower()[0] == 'd':
            self.dispatcher.config.load_dist(name)
        else:
            print "Invalid choice."
            return
        flag = raw_input("Load another configuration for this spreadsheet? (Y/n) ")
        if flag.lower()[0] == 'y':
            self.do_loadconf(line)
        os.system(clearcom)
        print self.intro
            

    def do_flush(self, args):
        '''\tFlushes autosaved files.'''
        flag = raw_input("Are you sure you want to delete all autosaved files? (Y/N) ").lower()[0]
        if flag == 'y':
            for f in os.listdir(os.getcwd()):
                if "autosave" in f:
                    os.remove(f)
        print "Done."

    def do_clear(self, args):
        '''\tClears the screen.'''
        os.system(clearcom)
        print self.intro

    def do_dropempty(self, args):
        '''\tRemoves empty columns from current file.'''
        self.dispatcher.toolbox.dropempty()
        print "Done."
    
    def do_load(self, name):
        '''\tLoads a file into memory to work with.'''
        try:
            try:
                os.makedirs(os.path.join(os.getcwd(), 'backup'))
            except OSError as e:
                if e.errno != errno.EEXIST:
                    raise
            try:
                os.makedirs(os.path.join(os.getcwd(), 'backup', str(self.dispatcher.session)))
            except OSError as e:
                if e.errno != errno.EEXIST:
                    raise
            self.dispatcher.load(name)
            self.prompt = "[" + self.dispatcher.working.wfile + "]> "
        except:
            print "Sorry, please try a different file."

    def do_loadall(self, line):
        '''\tLoads all compatible spreadsheet files in the directory into memory.'''
        for f in os.listdir(os.getcwd()):
            if (f[-4:] == '.csv' or f[-5:] == '.xlsx') and f not in self.dispatcher.list_active():
                self.do_load(f)

    def do_list(self, args):
        '''\tLists all currently active files.'''
        active = self.dispatcher.list_active()
        for item in active:
            print str(active.index(item) + 1) + ': ' + item

    def do_switch(self, name):
        '''\tSwitch active file. Takes as an argument either the full filename or its position on the active file list.'''
        try:
            name = int(name)
            name = self.dispatcher.list_active()[int(name)-1]
        except:
            name = name
        self.dispatcher.switch(name)
        self.prompt = "[" + name + "]> "

    def do_clean(self, line):
        '''\tClean, validate, and complete data in current file.'''
        if 'config' not in self.dispatcher.working.log:
            print "Configuration not set! Exiting..."
            return
        self.dispatcher.working.df = self.dispatcher.toolbox.clean(self.dispatcher.config)
        print 'Done.'

    def do_scatter(self, line):
        '''\tOutput a scatter plot of the data between two columns.'''
        self.do_columns(line)
        dim = raw_input("X,Y? ")
        args = [x.strip() for x in dim.split(',')]
        names = []
        for arg in args:
            try:
                num = int(arg)
                names.append(self.dispatcher.working.getcols()[num-1])
            except:
                names.append(arg)
        self.dispatcher.analyzer.simplescatter(names[0],names[1])

    def do_save(self, line):
        '''\tSave active file to disk.'''
        self.dispatcher.working.df.to_csv(self.dispatcher.working.wfile,index=False)
        print 'Done.'

    def do_rename(self, line):
        '''\tRename current working file to specified value.'''
        self.dispatcher.working.wfile = line
        self.prompt = "[" + line + "]> "

    def do_xlsx(self, line):
        '''\tSave file or list of files as sheets in Excel workbook.'''
        args = [x.strip() for x in line.split(',')]
        names = []
        for arg in args:
            try:
                num = int(arg)
                names.append(self.dispatcher.list_active()[num-1])
            except:
                names.append(arg)
        try:
            os.makedirs(os.path.join(os.getcwd(), '-'.join(os.path.splitext(names[0])[0].split('-')[:-1])))
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise
        writer = pd.ExcelWriter(os.path.join(os.getcwd(),'-'.join(os.path.splitext(names[0])[0].split('-')[:-1]),'excelworkbook.xlsx'), options={'encoding':'utf-8'})
        for name in names:
            print name
            handler = [x for x in self.dispatcher.handlers if x.wfile == name][0]
            print os.path.splitext(handler.wfile)[0].split('-')[-1]
            handler.df.to_excel(writer,os.path.splitext(handler.wfile)[0].split('-')[-1], index=False)
        writer.save()

    def do_saveall(self, line):
        '''\tSave all files in memory to disk.'''
        for handler in self.dispatcher.handlers:
            handler.df.to_csv(handler.wfile,index=False)
        print "Done."

    def do_unload(self, line):
        '''\tUnloads current working file from memory. Save before using.'''
        flag = raw_input("Are you sure you want to stop working with " + self.dispatcher.working.wfile +"? ").lower()[0]
        if flag == 'y':
            self.prompt = '[+++]> '
            self.dispatcher.handlers.remove(self.dispatcher.working)
            self.dispatcher.modifiers.remove(self.dispatcher.toolbox)
            self.dispatcher.configs.remove(self.dispatcher.config)
            self.dispatcher.analyzers.remove(self.dispatcher.analyzer)
            self.dispatcher.working = None
            self.dispatcher.toolbox = None
            self.dispatcher.config = None
            self.dispatcher.analyzer = None

    def do_columns(self, line):
        '''\tReturns column headers for active file with positions.'''
        print "Columns:\n\t"
        columns = self.dispatcher.working.getcols()
        for column in columns:
            print '\t' + str(columns.index(column) + 1) + ': ' + column
        
    def do_dump(self, line):
        '''\tRemoves files from working memory. Save before using. Name which files via either full name or position on active file list. No argument drops all files.'''
        args = [x.strip() for x in line.split(',')]
        if args:
            names = []
            for arg in args:
                try:
                    num = int(arg)
                    names.append(self.dispatcher.list_active()[num-1])
                except:
                    names.append(arg)
            flag = raw_input("Are you sure you want to unload these files:\n\t" + '\n\t'.join(names)+'? (Y/N) ').lower()[0]
            if flag == 'y':
                for name in names:
                    handler = [x for x in self.dispatcher.handlers if x.wfile == name][0]
                    if self.dispatcher.working == handler:
                        self.dispatcher.working = None
                        self.dispatcher.toolbox = None
                        self.dispatcher.config = None
                        self.dispatcher.analyzer = None
                        self.prompt = '[+++]> '
                    self.dispatcher.handlers.remove(handler)
                    self.dispatcher.modifiers.remove([x for x in self.dispatcher.modifiers if x.handler == handler][0])
                    self.dispatcher.configs.remove([x for x in self.dispatcher.configs if x.handler == handler][0])
                    self.dispatcher.analyzers.remove([x for x in self.dispatcher.analyzers if x.handler == handler][0])
                for f in os.listdir(os.getcwd()):
                    if "autosave" in f:
                        os.remove(f)
                print "Done."
                return
                
        else:
            flag = raw_input("Are you sure you want to unload all files from memory? (Y/N) ").lower()[0]
            if flag == 'y':
                del self.dispatcher.handlers[:]
                del self.dispatcher.modifiers[:]
                del self.dispatcher.configs[:]
                del self.dispatcher.analyzers[:]
                self.dispatcher.working = None
                self.dispatcher.toolbox = None
                self.dispatcher.config = None
                self.dispatcher.analyzer = None
                self.prompt = '[+++]> '
                print "Done."
                return

    def do_config(self, args):
        '''\tPrint current configuration and reconfigure if desired.'''
        os.system(clearcom)
        print
        print "Distinguished:\n"
        self.dispatcher.config.print_dist()
        print "\n"
        print "Corrections:\n"
        self.dispatcher.config.print_corr()
        print
        flag = raw_input("Configure now? (dist/corr/both/no)")
        if flag[0].lower() == 'd':
            self.dispatcher.config.input_dist()
            self.dispatcher.working.log.append('config')
            self.dispatcher.working.hist.append(self.dispatcher.working.df.copy())
        elif flag[0].lower() == 'c':
            self.dispatcher.config.input_corr()
            self.dispatcher.working.log.append('config')
            self.dispatcher.working.hist.append(self.dispatcher.working.df.copy())
        elif flag[0].lower() == 'b':
            self.dispatcher.config.input_dist()
            self.dispatcher.config.input_corr()
            self.dispatcher.working.log.append('config')
            self.dispatcher.working.hist.append(self.dispatcher.working.df.copy())
        os.system(clearcom)
        print self.intro

    def do_correct(self, line):
        '''\tApply corrections from config to active file.'''
        self.dispatcher.toolbox.correct(self.dispatcher.config.corr)
        print 'Done.'

    def do_history(self, line):
        '''\tDisplays list of changes made to current file in current session, in order.'''
        print ", ".join(self.dispatcher.working.log)

    def do_undo(self, line):
        '''\tUndoes the last modification to the current working file.'''
        print "Undoing last change..."
        del self.dispatcher.working.hist[-1]
        del self.dispatcher.working.log[-1]
        self.dispatcher.working.df = self.dispatcher.working.hist[-1]
        print 'Done.'

    def do_singleton(self, col):
        '''\tSeparates records into two files: most recent occurence and all others. Column can be given by name or position.'''
        try:
            pos = int(col)
            col = self.dispatcher.working.getcols()[pos-1]
        except:
            col = col
        (singleton, duplicate) = self.dispatcher.toolbox.singleton(col)
        self.dispatcher.addHandler(CSVHandler(os.path.splitext(self.dispatcher.working.wfile)[0] + '-' + col + '-singleton.csv',singleton))
        self.dispatcher.addHandler(CSVHandler(os.path.splitext(self.dispatcher.working.wfile)[0] + '-' + col + '-duplicate.csv',duplicate))
        print "New files in memory. Current list:"
        print ', '.join(self.dispatcher.list_active())

    def do_split(self, col):
        '''\tSplits records based on values in a given column. Column can be given by name or position.\nThis can put a large load on memory, so be careful.'''
        try:
            pos = int(col)
            col = self.dispatcher.working.getcols()[pos-1]
        except:
            col = col
        categories = self.dispatcher.toolbox.split(col)
        for category in categories:
            self.dispatcher.addHandler(CSVHandler(os.path.splitext(self.dispatcher.working.wfile)[0] + '-' + category[1] + '.csv', category[0]))
        print "New files in memory. Current list:"
        print ', '.join(self.dispatcher.list_active())
        if raw_input("Save and clear memory for better performance? ").lower() == 'y':
            for handler in self.dispatcher.handlers:
                handler.df.to_csv(handler.wfile,index=False)
            del self.dispatcher.handlers[:]
            del self.dispatcher.modifiers[:]
            del self.dispatcher.configs[:]
            del self.dispatcher.analyzers[:]
            self.dispatcher.config = None
            self.dispatcher.working = None
            self.dispatcher.toolbox = None
            self.dispatcher.analyzer = None
            print "Memory cleared. Please load files."
            self.prompt = "[+++]> "

    def postcmd(self, stop, line):
        if self.dispatcher.handlers:
            for handler in self.dispatcher.handlers:
                try:
                    handler.hist[-1].to_csv(os.path.splitext(handler.wfile)[0] + ' - autosave.csv', index=False)
                except:
                    continue
        print
        return cmd.Cmd.postcmd(self, stop, line)

    def postloop(self):
        print "Removing temporary files..."
        for f in os.listdir(os.getcwd()):
            if "autosave" in f:
                os.remove(f)
        cmd.Cmd.postloop(self)
        print "Exiting..."
    
if __name__ == '__main__':
    GruntShell().cmdloop()
